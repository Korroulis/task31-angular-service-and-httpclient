# Task31

Create a new Angular Service called AuthService

## Auth Service

The auth service must have to methods

- register(user): Promise<any>
The register method should send an HTTP request using the HttpClient to register a user on the system 

- login(user): Promise<any>
The login method must send an HTTP request using the HttpClient to attempt a login using provided credentials.

*NOTE*: The user object for both methods must contain a username and password properties.  Please see the provided API documentation for more detail:
https://documenter.getpostman.com/view/1167753/Szf6Y8xS?version=latest

## Components (Login, Register)
You must inject the service into the register and login components you created in the previous task. 

The login component should execute the login method from the AuthService when the user clicks on the login button

The register component should execute the register method from the AuthService when the user clicks on the register button.