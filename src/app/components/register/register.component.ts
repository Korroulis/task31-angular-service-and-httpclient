import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    username:'',
    password:'',
    confirmPassword:''
  };

  @Output () signupEvent: EventEmitter<string> = new EventEmitter();

  public signupTitle: string = 'Register to the super duper app';

  constructor(private authService: AuthService, private router: Router, private session:SessionService ) { }

  ngOnInit(): void {
  }

  async SignupClick() {

    try {

      // Add the user in the servers ony if the passwords match and are larger than 8 letters

      if (this.user.password===this.user.confirmPassword && this.user.password.length>8) {
        const result: any = await this.authService.register(this.user);
        console.log(result);
        if (result.status<400) {
          this.session.save({
            token: result.data.token,
            username: result.data.user.username
          });
          this.router.navigateByUrl('/dashboard');
        }
        //this.router.navigateByUrl('/dashboard');

      } else if (this.user.password===this.user.confirmPassword && this.user.password.length<9 ) {
        alert("Please enter a password of 9 or more letters");
      }
      else {
        alert("Passwords don't match");
      }

    } catch (e){

    }
    
  }

}
