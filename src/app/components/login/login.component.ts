import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userLogin = {
    username:'',
    password:'',
  };

  @Output () loginEvent: EventEmitter<string> = new EventEmitter();

  public loginTitle: string = 'Login to the super duper app';
  //private Result: any

  constructor(private authService: AuthService, private router: Router, private session: SessionService) { }

  ngOnInit(): void {

  }

  loginClick() {

    //const result
    const success = this.authService.login(this.userLogin);
    /* if (this.user.password===this.user.confirmPassword && this.user.password.length>8) {
      const result: any = await this.authService.register(this.user);

    if (result.status <400) {
      this.session.save({token: result.data.token, username: result.data.user.username});
      this.router.navigateByUrl('/dashboard');
    } */

    if (success) {
      this.router.navigateByUrl('/dashboard');
    } else {
      alert("You are not authorized to enter");
    }
    
  }

}
