import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: any = null;
  private userLogin: any = null;

  constructor(private http: HttpClient) { }

  register(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/register', {
      user: {
        username: user.username,
        password:user.password
      }
    }).toPromise()
  }

  /* public login(userLogin:any):boolean { */
    login(user): Promise<any> {
      return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/login', {
        user: {...user
        }
      }).toPromise()
    }


  public isLoggedIn(): boolean {
    return this.userLogin !== null;
  }

}
